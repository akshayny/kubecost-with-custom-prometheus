resource "helm_release" "cluster_prometheus" {
    atomic = true
    name = "prometheus"
    repository = "https://prometheus-community.github.io/helm-charts"
    chart = "kube-prometheus-stack"
    version = "33.1.0"
    create_namespace = true
    namespace = "monitoring"
set {

name = "prometheus.prometheusSpec.additionalScrapeConfigs"

value = yamlencode([{"job_name": "kubecost\nhonor_labels: true\nscrape_interval: 1m\nscrape_timeout: 10s\nmetrics_path: /metrics\nscheme: http\ndns_sd_configs:\n- names:\n — kubecost-cost-analyzer.kubecost\n type: ‘A’\n port: 9003"}])

}
}