provider "aws" {
  region = var.region
  access_key = ""
  secret_key = ""
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "t"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}
