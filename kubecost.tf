    resource "helm_release" "kubecost" {
    name = "kubecost"
    repository = "https://kubecost.github.io/cost-analyzer"
    chart = "cost-analyzer"
    version = "1.101.2"
    namespace = "kubecost"
    create_namespace = "true"
    set {
    name = "global.prometheus.enabled"
    value = "false"
    }
    set {
    name = "global.prometheus.fqdn"
    value = "http://prometheus-kube-prometheus-prometheus.monitoring.svc:9090"
    }
    set {
    name = "prometheus.nodeExporter.enabled"
    value = "false"
    }
    set {
    name = "prometheus.kube-state-metrics.disabled"
    value = "true"
    }
    set {
    name = "prometheus.serviceAccounts.nodeExporter.create"
    value = "false"
    }
    depends_on = [
    helm_release.cluster_prometheus
    ]
    }